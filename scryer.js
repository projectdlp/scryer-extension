var collectorUrl = "https://scryer.darklordpotter.net/pensieve/bookmark"
var loadTime = new Date();

// get the starting date
var eventId = generateId();
var start = new Date();
var timeStart = 0;
var lastScroll = start.getTime();
var scrolls = [];
 
// capture the start time with a mouseover on the document body
$(document).ready(function(){
    // Handy: remove the selection prevention style
    $("#storytextp").css("-webkit-user-select", "");

    $("body").mouseover(function(){
        if (timeStart==0) {
            timeStart = new Date().getTime();
        }
    });

    if (isNot404()) {
        postEvent("chapter_load", buildPageLoadEvent());


        $(window).on("beforeunload", function() {
            recordScroll();
            postEvent("chapter_exit", buildPageExitEvent());
        });
    }
});

function isNot404() {
    msg = $(".gui_warning");

    if (msg == null) return true;

    return msg.text().indexOf("Code 1") == -1;
}

recordScroll = function() { 
    endTime = new Date().getTime();
    scrollPosition = (document.body.scrollTop+document.body.offsetHeight)/document.body.scrollHeight;
    scrolls.push({start: lastScroll, end: endTime, scrollPct: scrollPosition});
    lastScroll = endTime;
};

$(window).on("scrollstop", {latency: 30000}, recordScroll);

function generateId() {
    return Math.floor(Math.random() * 2147483648    );
}

function postEvent(event_name, e) {
    var eventPayload = $.extend({event_name: event_name, event_id: eventId}, e);

    chrome.runtime.sendMessage(eventPayload, function(response) {
      console.log(response);
    });
}

function buildPageExitEvent() {
    return {
        url: document.URL,
        at_end: atEndOfPage(),
        timing: {
            start: start.getTime(),
            interact_start: timeStart,
            end: new Date().getTime()
        },
        scroll_times: scrolls
    }
}

function atEndOfPage() {
    return $(window).scrollTop() >= $(document).height() - $(window).height() - 1000;
}

function buildPageLoadEvent() {
    wordcount = $("#storytextp").text().replace(/\s+/gi, ' ').split(' ').length;

    summary = $("#profile_top > div").text();
    storydata = $("#profile_top > span.xgray").text();
    chapterSelector = document.getElementById('chap_select');
    authorName = $("#profile_top > a[href^='/u/']").text();
    authorId = parseInt($("#profile_top > a[href^='/u/']").attr("href").split("/")[2]);

    return {
        url: document.URL,
        timestamp: start.getTime(),
        story: {
            id: parseInt(document.URL.split("/")[4]), // ffn JS
            name: $("#profile_top > b").text(), // ffn JS
            summary: summary,
            story_metadata: storydata,
            chapters: chapterSelector.options.length
        },
        author: {
            id: authorId, // ffn JS
            name: authorName,
        },
        chapter: {
            name: chapterSelector.options[chapterSelector.selectedIndex].innerHTML,
            num: chapterSelector.value,
            wordcount: wordcount
        }
    }
} 