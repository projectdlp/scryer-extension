// SAMPLE
this.manifest = {
    "name": "Scryer Pensieve",
    "icon": "icon.png",
    "settings": [
        // {
        //     "tab": i18n.get("information"),
        //     "group": i18n.get("login"),
        //     "name": "email",
        //     "type": "text",
        //     "label": i18n.get("email"),
        //     "text": i18n.get("x-characters")
        // },
        {
            "tab": i18n.get("information"),
            "group": i18n.get("login"),
            "name": "password",
            "type": "text",
            "label": i18n.get("API Token"),
            "text": i18n.get("x-characters-pw"),
            "masked": true
        },
        // {
        //     "tab": i18n.get("information"),
        //     "group": i18n.get("login"),
        //     "name": "myDescription",
        //     "type": "description",
        //     "text": i18n.get("description")
        // },
        // {
        //     "tab": i18n.get("information"),
        //     "group": i18n.get("logout"),
        //     "name": "myButton",
        //     "type": "button",
        //     "label": "",
        //     "text": i18n.get("logout")
        // }
    ],
    "alignment": [
        [
            // "email",
            "password"
        ]
    ]
};
