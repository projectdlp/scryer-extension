var collectorUrl = "https://scryer.darklordpotter.net/pensieve/bookmark"
var settings = new Store("settings");

chrome.runtime.onMessage.addListener(
  function(eventPayload, sender, sendResponse) {
    var email = settings.get("email");
    var password = settings.get("password");

    console.log("Sending message with : " + email);

    $.ajax({
        url: collectorUrl,
        type: "post",
        dataType: 'json',
        accept: "application/json",
        contentType: "application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Token " + password);
        },
        data: JSON.stringify(eventPayload),
        async:false,
        statusCode: {
            401: function (){
                console.log('Scryer: login required.');
                // chrome.tabs.create({url:"popup.html"});
            },
            200: function() {
                console.log('Scryer: Posted page event ('+JSON.stringify(eventPayload)+')');
            }
        }
    });
 });
